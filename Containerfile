FROM rockylinux/rockylinux:9.4 AS base

LABEL org.opencontainers.image.authors="contributions@ingeniorskap.no"

# Activate necessary repos
RUN set -eux; \
    \
    dnf install -y --refresh \
        https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-9.noarch.rpm \
    & \
    \
    dnf install -y --refresh \
        https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-9.noarch.rpm

# Install dependencies
FROM base AS sys-deps
RUN set -eux; \
    \
    dnf install -y --refresh \
        ffmpeg \
        python3.11 \
        python3-pip-wheel\
        git \
        git-lfs\
        && \
        dnf clean all \
        && \
        python3.11 -m ensurepip --upgrade


# install large and slow-moving python deps in layer for better caching
FROM sys-deps AS py-deps
RUN set -eux; \
    \
    python3.11 -m pip install torch

# final layer
FROM py-deps AS predalert

ENV PREDALERT_CONFIG_DIR /config

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES="compute,video,utility"
EXPOSE 8443

# Install program
RUN set -eux; \
    \
    python3.11 -m pip install predalert@git+https://gitlab.com/papiris/predator-detect-and-notify@feat-containerize

WORKDIR /

COPY ./predalert/web/static predalert/web/static

COPY ./predalert/web/templates predalert/web/templates

# Set program to run on startup
CMD ["python3.11", "-m", "predalert"]

from diagrams import Diagram, Cluster
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Client, User
from diagrams.onprem.network import Internet
from diagrams.programming.flowchart import Action, Decision, Database
from diagrams.programming.framework import Fastapi, Starlette
from diagrams.programming.language import Python, Javascript

from diagrams.c4 import Person, Container, Database, System, SystemBoundary, Relationship

graph_attr = {
    "splines": "spline",
    "fontsize": "24",
}


with Diagram("Videobehandling systemdiagram", direction="TB", graph_attr=graph_attr, outformat="pdf"):
    # user = Person(
        # # name="Bonde", description="Bonden på gården"
    # )

    with SystemBoundary("Videoprosessering", fontsize="16"):
        ingest_sources = Container(
            name="Videoinnhenting",
            technology="Deffcode, RTSP",
            description="Dekoder videostrømmer til bilder",
        )

        detection = Container(
            name="Bildeanalyse",
            technology="RTDETR, OpenCV, Numpy",
            description="Ser etter gaupe på bilder, \nmarkerer område,\ntranskoder ut-bilder for nett",
        )

    with SystemBoundary("Nettbasert brukergrensesnitt", fontsize="16"):
        web_backend = Container(
            name="Serverlogikk",
            technology="SSE-Starlette, FastAPI",
            description="Tilbyr SSE og dynamiske baner \nfor bilder"
            )

        web_frontend = Container(
            name="Klientlogikk",
            technology="Javascript",
            description="Transkoder bilder fra tekst \nog presenterer dem"
            )

    with SystemBoundary("Lokalt brukergrensesnitt", fontsize="16"):
        desktop_ui = Container(
            name="Lokalt brukergrensesnitt",
            technology="OpenCV",
            description="Viser bilder lokalt på maskin"
            )

        image_grid = Container(
            name="Bildesammenføyer",
            technology="Numpy, OpenCV",
            description="Syr bilder\nsammen i rutenett"
            )


    camera = System(
        name="Overvåkningskamera",
        technology="RTSP",
        description="Strømmer video fra sauegjerde",
        external=True)


    # Web relationships
    # user >> Relationship("Besøker nettside, \nser på video") >> web_frontend
    web_frontend << Relationship("Gir tekst-transkodede bilder til") << web_backend
    web_backend << Relationship("Gir tekst-transkodede bilder til", minlen="1.6") << detection

    # Desktop relationships
    # user >> Relationship("Ser på video") >> desktop_ui
    image_grid >> Relationship("Gir sammenføyde \nbilder til") >> desktop_ui
    detection >> Relationship("Gir bilder til") >> image_grid


    # video rels
    ingest_sources >> Relationship("Henter video fra") >> camera
    ingest_sources >> Relationship("Gir bilder til") >> detection

from diagrams import Diagram
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Client, User
from diagrams.onprem.network import Internet
from diagrams.programming.flowchart import Action, Decision, Database
from diagrams.programming.framework import Fastapi, Starlette
from diagrams.programming.language import Python, Javascript

from diagrams.c4 import Person, Container, Database, System, SystemBoundary, Relationship

graph_attr = {
    "splines": "spline",
}

with Diagram("Konteiner diagram over lokal funksjonalitet",
             direction="TB",
             outformat="pdf",
             graph_attr=graph_attr):

    bruker = Person(
        name="Bonde",
        description="Bonden på gården",
    )

    with SystemBoundary("Funksjonalitet lokalt på maskin"):
        desktop_ui = Container(
            name="Lokalt brukergrensesnitt",
            technology="OpenCV, Numpy",
            description="Lager rutenett av analyserte bilder og viser det lokalt på skjerm",
            fontsize="20",
            )

        desktop_alert = Container(
            name="Lokal varsler",
            technology="Python, Desktop-notifier",
            description="Viser sprettoppvarsler lokalt på maskinen ved hendelser",
            )

        desktop_image_grid = Container(
            name="Sammenføyer for bilder",
            technology="Python, OpenCV, Numpy",
            description="Lager rutenett av analyserte bilder",
            )

    detection = System(
        name="Videobehandling",
        description="Innhenting og analyse av videostrøm",
        )

    camera = System(
        name="Overvåkningskamera",
        description="Strømmer video fra sauegjerdet",
        external=True
        )


    bruker >> Relationship("Ser på kameraovervåkningen med") >> desktop_ui
    detection >> Relationship("Gir gjenkjenningsdetaljer til") >> desktop_alert
    bruker >> Relationship("Mottar lokale varsler fra") >> desktop_alert
    detection >> Relationship("Gir analyserte bilder til") >> desktop_image_grid
    desktop_image_grid >> Relationship("Gir rutenett av bilder til") >> desktop_ui
    detection >> Relationship("Henter videostrøm fra") >> camera


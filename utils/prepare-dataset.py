#!/bin/env/ python3


#-------------------------#

# image-dataset-preparation.py
# Copyright (c) 2023 Jacob Dybvald Ludvigsen (jd_lud at pm dot me)

#-------------------------#

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

#------------------------#

import numpy as np
import imgaug as ia
import imgaug.augmenters as iaa
#from collections import defaultdict
#from torchvision.transforms.v2 import functional as F

from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
#import PIL.Image

import torch
import torch.utils.data

import torchvision
import argparse
#import pathlib as Path
#import sys


from pathlib import Path
import cv2

'''
def main():
    destination_path = './datasets/Sheeps-PascalVOC-export/'
    target_path = './datasets/Sheeps-PascalVOC-export/rsz/'

    format_of_your_images = 'jpg'

    all_the_files = Path(destination_path).rglob(f'*.{format_of_your_images}')

    for f in all_the_files:
        p = cv2.imread(str(f))
        cv2.resize(p
        #  transformation
        cv2.imwrite(f'{target_path}/{f.name}', p)





def augment_images_and_bbs(images, bbs):






if isinstance(image, PIL.Image.Image):
        image = F.to_image_tensor(image)
    image = F.convert_dtype(image, torch.uint8)

def path_to_array(root_dir):
    for

'''

def main():
    ### Parse commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(nargs='*', action='store', dest='source', \
        default='None', help='root directory for images and labels folder')
    args = parser.parse_args()
    root_dir = Path(args.source[0])

    print(root_dir)

    #images_dir = root_dir
    format_of_your_images ="jpg"

    #ia.seed(2)
    seq = iaa.Sequential([
        #iaa.Fliplr(0.5), # horizontal flips
        #iaa.Crop(percent=(0, 0.1)), # random crops
        # Small gaussian blur with random sigma between 0 and 0.5.
        # But we only blur about 50% of all images.
        iaa.Sometimes(
            0.5,
            iaa.GaussianBlur(sigma=(0, 0.5))
        ),
        # Strengthen or weaken the contrast in each image.
        iaa.LinearContrast((0.75, 1.5)),
        # Add gaussian noise.
        # For 50% of all images, we sample the noise once per pixel.
        # For the other 50% of all images, we sample the noise per pixel AND
        # channel. This can change the color (not only brightness) of the
        # pixels.
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
        # Make some images brighter and some darker.
        # In 20% of all cases, we sample the multiplier once per channel,
        # which can end up changing the color of the images.
        iaa.Multiply((0.8, 1.2), per_channel=0.2),
        # Apply affine transformations to each image.
        # Scale/zoom them, translate/move them, rotate them and shear them.
        #  #iaa.Affine(
        #     scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
        #     translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
        #     rotate=(-25, 25),
        #     shear=(-8, 8)
        # )
    ], random_order=True) # apply augmenters in random order


    all_the_images = Path(root_dir).rglob(f'*.*')
    all_the_labels = Path(root_dir).rglob(f'*.txt')
    #print(all_the_labels)

    for i in all_the_images:
        i = str(i)
        #print(i)
        im = cv2.imread(i)
        #if im.shape !=(640,640,3):
            #breakpoint()
        #with open(str(l), "r") as f:
        #    lr = f.readlines()
            #print(lr)
        # standard way of augmenting segmentation maps via augment_segmentation_maps()
        image_aug = seq(image=im)
            #for im in image_aug:
                # #print(im)

                # resized = cv2.resize(src=im, dsize=(640,640), interpolation = cv2.INTER_LINEAR)
        cv2.imwrite(f'{i}.new.jpg', image_aug)
        #with open(bbs_aug.name, "w") as f:
        #    f.write(bbs_aug)

    #results = augment_images_and_bbs(seq, all_the_images, all_the_labels)





#
# # We are using BETA APIs, so we deactivate the associated warning, thereby acknowledging that
# # some APIs may slightly change in the future
# torchvision.disable_beta_transforms_warning()
#
# from torchvision import models, datasets
# import torchvision.transforms.v2 as transforms
#
if __name__ == '__main__':
    main()

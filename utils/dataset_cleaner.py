#!/usr/bin/env python3

import argparse
import pathlib as path
import re

classes_to_keep= xrange(0, 23)
p=re.compile(^[0-9]|1[0-9]|2[0-3], re.MULTILINE)

def scrub_files(sourcedir):
    for fn in sourcedir:
        out_lines= []
        with open(fn, "r") as  f:
            lines=f.readlines()
            out_lines.append(re.match(p, lines))
#
#             for line in lines:
#
#                 first_word=lines.split(" ")[0]
#                 first_number = int(first_word)
#                 if first_number in classes_to_keep:
#                     out_lines.append(line)
        with open(f'{fn}.scrubbed.txt', "w") as f:
            f.write(out_lines)




def main():
    ### Parse commandline arguments
    parser = argparse.ArgumentParser() #'-s', '--sources',
    parser.add_argument(nargs='*', action='store', dest='sourcedir', \
        default='0', help='source directory of labels to scrub')
    args = parser.parse_args()
    sourcedir = args.sourcedir

    scrub_files(sourcedir)

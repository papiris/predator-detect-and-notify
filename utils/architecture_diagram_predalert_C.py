from diagrams import Diagram, Cluster
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Client, User
from diagrams.onprem.network import Internet
from diagrams.programming.flowchart import Action, Decision, Database
from diagrams.programming.framework import Fastapi, Starlette
from diagrams.programming.language import Python, Javascript

from diagrams.c4 import Person, Container, Database, System, SystemBoundary, Relationship, C4Node

graph_attr = {
    "splines": "spline",
    "fontsize": "24",
}


with Diagram("Funksjonsdiagram for Predalert", direction="LR", graph_attr=graph_attr, outformat="pdf"):
    user = Person(
        name="Bonde", description="Bonden på gården"
    )

    with SystemBoundary("Predalert", fontsize="16"):

        desktop = Container(
            name="Lokal funksjonalitet",
            technology="OpenCV, Numpy, Desktop-notifier",
            description="Presenterer analyserte videoer \nog varsler lokalt på maskin",
            )

        video_processing = Container(
            name="Videobehandling",
            technology="Deffcode, RTSP, RTDETR",
            description="Innhenter og analyserer videostrøm",
            )

        web = Container(
            width="3",
            name="Nettbasert funksjonalitet",
            technology="JS, WebPush, SSE, Ziti, FastAPI, SQL",
            description="Presenterer analyserte videoer \npå nettside og sender varsler",
        )

        main = Container(
            name="Hovedprosess",
            technology="Python",
            description="Kontrollerer alle underprosesser \nog felles ressurser"
            )

        log = Container(
            name="Loggføring",
            technology="Python",
            description="Fører felles logg for alle prosesser"
            )


    camera = System(
        name="Overvåkningskamera",
        technology="RTSP",
        description="Strømmer video fra sauegjerdet",
        external=True)


    # relationships
    user >> Relationship("Ser på videostrøm med og mottar varsler fra") >> [desktop, web]
    video_processing >> Relationship("Gir analyserte bilder og varselinfo til") >> [web, desktop]
    video_processing >> Relationship("Henter videostrøm fra") >> camera
    # main >> Relationship("Kontrollerer") >> [log, web, video_processing, desktop]
    # log >> Relationship("Logger for") >> [main, web, video_processing, desktop]


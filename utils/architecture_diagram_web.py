from diagrams import Diagram, Cluster
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Client, User
from diagrams.onprem.network import Internet
from diagrams.programming.flowchart import Action, Decision, Database
from diagrams.programming.framework import Fastapi, Starlette
from diagrams.programming.language import Python, Javascript

from diagrams.c4 import Person, Container, Database, System, SystemBoundary, Relationship

graph_attr = {
    "splines": "spline",
    "fontsize": "24",
}


with Diagram("C4 konteiner diagram for nettfunksjonalitet", direction="TB", graph_attr=graph_attr, outformat="pdf"):
    user = Person(
        name="Bonde",
        description="Bonden på gården",
    )

    with SystemBoundary("Nettfunksjonalitet", fontsize="16"):
        webapp = Container(
            name="Brukergrensesnitt",
            technology="HTML, Javascript",
            description="Presenterer statisk innhold\n og applikasjonens nettside",
        )

        webapp_backend = Container(
            name="Tilknyttet serverlogikk",
            technology="OpenZiti, SSE, Hypercorn, FastAPI",
            description="Tilbyr nettapplikasjonens \nfunksjonalitet via Hypercorn, API \nog SSE-Starlette",
        )

        database = Database(
            name="Database",
            technology="SQLModel, SQLite",
            description="Lagrer brukerens \nabbonnementsinformasjon \nfor push-varsler",
        )

    push = Container(
        name="WebPush system",
        technology="WebPush",
        description="Sender WebPush varsler \ntil abonnenter \nved hendelser")

    detection = System(
        name="Videobehandling",
        description="Innhenting og analyse av videostrøm",
        )

    camera = System(
        name="Overvåkningskamera",
        description="Strømmer video fra sauegjerdet",
        external=True
        )

    # Web relationships
    user >> Relationship("Besøker nettsiden, \nser på kamera \nog abbonnerer \npå varsler", minlen="2") >> webapp
    webapp >> Relationship("Får video av, \ngjør API-kall til [JSON/HTTPS]") >> webapp_backend
    webapp_backend >> Relationship("Leser fra og skriver til") >> database
    push >> Relationship("Leser abonnementsinformasjon fra") >> database
    user << Relationship("Sender push-varsler til", minlen="1.5") << push

    # Other rels
    detection >> Relationship("Gir gjenkjenningsdetaljer til") >> push
    detection >> Relationship("Gir analyserte bilder til") >> webapp_backend
    detection >> Relationship("Henter videostrøm fra") >> camera

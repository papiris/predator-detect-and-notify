#!/usr/bin/env python3
from wsdiscovery.discovery import ThreadedWSDiscovery as WSDiscovery
from wsdiscovery import QName, Scope
import pyonvif
import re
"""
def discover_onvif():
    wsd = WSDiscovery()
    wsd.start()
    scope1 = Scope("onvif://www.onvif.org/type/NetworkVideoTransmitter")
    services = wsd.searchServices()

    ipaddresses = []
    for service in services:
    #filter those devices that dont have ONVIF service
        print(service.getTypes()[0])
        ipaddress = re.search('(\d+|\.)+', str(service.getXAddrs()[0])).group(0)
        ipaddresses.append(ipaddress)
        print(f'scopes for the service: {display(service.getScopes())}')
        print('----------END')

    print(f'\nnumber of devices detected: {len(services)}')
    wsd.stop()
    return ipaddresses
"""
def discover_onvif():
    discoo = pyonvif.OnvifCam()
    discovered = discoo.discover()
    print(discovered)
    return discovered

def recoverStream(profileToken):
    # pseudo-code for recovery of lost stream. based on https://www.onvif.org/specs/srv/media/ONVIF-Media-Service-Spec.pdf?441d4a&441d4a#%5B%7B%22num%22%3A384%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C56.692%2C411.205%2Cnull%5D

    pyonvif.OnvifCam.sendSoapMsg(SetSynchronizationPoint())


def display(discovered_list):
    for item in discovered_list:
        print(item)

def main():
    print("started")

    discovered_list = discover_onvif()
    display(discovered_list)

    #pyonvif.OnvifCam.sendSoapMsg(GET_PROFILES)


if __name__ == "__main__":
    main()

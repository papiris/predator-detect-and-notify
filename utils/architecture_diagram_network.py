from diagrams import Diagram, Cluster, Edge, Node
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Client, User
from diagrams.onprem.network import Internet
from diagrams.programming.flowchart import Action, Decision, Database
from diagrams.programming.framework import Fastapi, Starlette
from diagrams.programming.language import Python, Javascript
from diagrams.generic.device import Mobile
from diagrams.custom import Custom

# from diagrams.c4 import Person, Container, Database, System, SystemBoundary, Relationship

graph_attr = {
    "splines": "spline",
    "fontsize": "24",
}


with Diagram("Diagram over nettverksarkitektur", direction="TB", graph_attr=graph_attr, outformat="pdf"):

    with Cluster("Internt i programmet"):
        prosess = Server("Prosess for \nnettsidefunksjonalitet")
        ziti_sdk = Custom("OpenZiti Python SDK", "./assets/openziti_py.png")

    wp_server = Server("Nettleserens valgte \nWebPush tjeneste")
    webpush = Custom("Webpush varsel", "./assets/message.png")

    pc = Client("Autentisert enhet")
    mobile = Mobile("Autentisert enhet")


    prosess >> wp_server >> webpush >> [pc, mobile]



    prosess << Edge() >> ziti_sdk << Edge() >> Internet("Ziti nettverk") << Edge() >> [pc, mobile] << Edge() >> User("Bruker")

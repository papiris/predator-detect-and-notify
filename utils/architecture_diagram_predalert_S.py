from diagrams import Diagram, Cluster
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Client, User
from diagrams.onprem.network import Internet
from diagrams.programming.flowchart import Action, Decision, Database
from diagrams.programming.framework import Fastapi, Starlette
from diagrams.programming.language import Python, Javascript

from diagrams.c4 import Person, Container, Database, System, SystemBoundary, Relationship, C4Node

graph_attr = {
    "splines": "spline",
    "fontsize": "24",
}


with Diagram("C4 system diagram for Predalert", direction="TB", graph_attr=graph_attr, outformat="pdf"):
    user = Person(
        name="Bonde", description="Bonden på gården"
    )

    predalert = System(name="Predalert", description="Analyserer video fra overvåkningskameraer, lar brukere se video og få varsler")

    with Cluster("Kameraer på gården"):
        camera1 = System(name="Overvåkningskamera", description="Strømmer video fra sauegjerdet", external=True)
        camera2 = System(name="Overvåkningskamera", description="Strømmer video fra sauegjerdet", external=True)

    # relationships
    user >> Relationship("Ser på kameraovervåkningen med og mottar varsler fra") >> predalert
    predalert >> Relationship("Henter video hos") >> [camera1, camera2]

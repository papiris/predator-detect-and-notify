#!/bin/env python3

import pathlib as Path
import argparse
import glob
import re


parser = argparse.ArgumentParser() #'-s', '--sources',
parser.add_argument(nargs='*', action='store', dest='sources', \
        default='0', help='sources')
args = parser.parse_args()
root_dir = str(args.sources[0])
labels = (f'{root_dir}/labels')
print(labels)

txt_files = glob.glob(labels +'/*.txt')
print(txt_files[0])
for f in txt_files:
    with open(f) as fread:
        contents = fread.read()
        # contents = contents.replace('16 ', '10 ')
        # contents = contents.replace('14 ', '15 ')
        # contents = contents.replace('17 ', '16 ')
        # contents = contents.replace('19 ', '2 ')
        # contents = contents.replace(f'[0:1000] ', '16 ')
        contents = re.sub("[0-9][0-9]\s", '16 ', contents)
        # contents = contents.replace('18 ', '18 ')
        # contents = contents.replace('2 ', '90 ')
        # contents = contents.replace('8 ', '90 ')
        # contents = contents.replace('7 ', '90 ')

    with open(f, 'w') as fwrite:
        fwrite.write(contents)

# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


from sqlmodel import SQLModel, create_engine
from pathlib import Path
import tomllib
from os import environ

# Load configuration file
path = Path(environ['PREDALERT_CONFIG_DIR'], "config.toml")
try:
    with path.open("rb") as f:
        config_file = tomllib.load(f)
except tomllib.TOMLDecodeError as e:
    print(f"ERR: Config file couldn't load due to error: {e}")


# Configure database connection
engine = create_engine(config_file['database']['url'], echo=True)

# Create database and tables
def create_db_and_tables():
    SQLModel.metadata.create_all(engine)

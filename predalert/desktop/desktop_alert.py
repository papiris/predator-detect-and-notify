# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import time
from multiprocessing import Event, Queue

from desktop_notifier import DesktopNotifier


def desktop_notifications(q_notifications: Queue,
                          q_logging: Queue,
                          event: Event):
    """
    Shows a list of detected classes in a system notification.

    Parameters
    ----------
        q_notifications (mp.Manager.Queue): Each item is a list
            of detected objects in the analyzed image.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        event (mp.Manager.Event): Cross-process flag, to initiate termination.
    """
    notifier = DesktopNotifier()
    while not event.is_set():
        time.sleep(0.5)
        try:
            detected_cls = q_notifications.get(timeout=0.5)
        except Exception as e:
            q_logging.put(("debug", f"desktop_notifications had error {e}"),
                          timeout=0.5)
            continue
        else:
            summarized_list = [f"{detected_cls.count(item)} {item}"
                               for item in set(detected_cls)]
            notifier.send_sync(title='Predalert detection!',
                               message=f'Detected classes {summarized_list}')
            q_logging.put(("info", f"Detected classes {summarized_list}"),
                          timeout=1)

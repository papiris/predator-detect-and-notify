# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


from multiprocessing import Event, Queue

import cv2 as cv


def show_image_grid(q_img_grid_local: Queue,
                    q_logging: Queue,
                    event: Event):
    """
    Display dynamic grid of images locally on the desktop,
    update view whenever a new grid is available.

    Parameters
    ----------
        q_img_grid_local (mp.Manager.Queue): Images to show on the local UI.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        event (mp.Manager.Event): Cross-process flag, to initiate termination
    """
    q_logging.put(("info", "show_image_grid started"), timeout=1)

    cv.namedWindow('Streams', cv.WINDOW_NORMAL)  # Start the thread for analytics and display
    cv.setWindowProperty('Streams', cv.WND_PROP_VISIBLE, 0)  # Set the window property to make it non-blocking
    cv.startWindowThread()

    while not event.is_set():
        try:
            img_grid = q_img_grid_local.get(1)

            cv.imshow('Streams', img_grid)
            cv.waitKey(1)
            img_grid.clear()  # clear list of images to reduce memory consumption

        except Exception as e:
            q_logging.put(("debug",
                          f"desktop_ui has error {e}"), timeout=1)
            continue

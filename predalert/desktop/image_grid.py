# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later

import cv2 as cv
import numpy as np


def make_image_grid(config_file,
                    q_result_dict,
                    q_img_grid_local,
                    q_logging,
                    event):
    """
    runs as a subprocess and shows "video views" of annotated frames
    in a dynamically adaptive grid.

    Parameters
    ----------
        q_result_dict (dict of mp.Manager.Queue objects):
            Analyzed, annotated images to be gridified.
            One queue per analytics process.
        q_img_grid_local (mp.Manager.Queue): Images to show on the local UI.
        q_img_grid_web (mp.Manager.Queue): Images to show on the web UI.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        display_res (tuple): Desired (width, height) resolution of images
            to display.
        source_code_repo (str): URL of repository where the application's
            source code is hosted. This is required to comply with AGPLv3.
        event (mp.Manager.Event): Cross-process flag, to initiate termination
    """
    q_logging.put(("info", "make_image_grid started"), timeout=1)

    ncols = None
    while not event.is_set():
        images = []
        try:
            for src, q_result in q_result_dict.items():
                q_logging.put(
                    ("debug",
                     f"q_result for {src} has {q_result.qsize()} elements"
                     ), timeout=0.1)

                # Get image
                result = q_result.get(1)
                if result.any():
                    image_rz = cv.resize(
                        result, (config_file["display"]["resolution"])
                        )
                    images.append(image_rz)

        except KeyboardInterrupt:
            q_logging.put(("info", "exited display loop"), timeout=0.1)
            break

        except Exception as e:
            q_logging.put(
                ("warning", f"display_images has error in q_result.get(): {e}"
                 ), timeout=0.1)
            continue

        else:
            # if there are images, make grid which adapts to number of images
            if images:
                # number of columns can be predefined
                if not ncols:
                    factors = [i for i in range(1, len(images)+1)
                               if len(images) % i == 0]
                    ncols = (factors[len(factors) // 2]
                             if len(factors) else len(images) // 4 + 1)

                nrows = int(len(images) / ncols) + int(len(images) % ncols)
                try:
                    # Assuming all images have the same shape
                    image_height, image_width, _ = images[0].shape

                except Exception as e:
                    q_logging.put(
                        ("warning", f"show_image_grid has error in image_matrix: {e}"
                         ), timeout=0.1)

                grid_height = nrows * image_height
                grid_width = ncols * image_width
                grid = np.zeros((grid_height, grid_width, 3), dtype=np.uint8)

                # insert image in grid
                for i, image in enumerate(images):
                    if i >= nrows * ncols:
                        break

                    row = i // ncols
                    col = i % ncols
                    y_start = row * image_height
                    y_end = (row + 1) * image_height
                    x_start = col * image_width
                    x_end = (col + 1) * image_width
                    grid[y_start:y_end, x_start:x_end] = image

                # show source code repo to comply with AGPLv3
                img_grid = cv.putText(grid,
                                      config_file["repo"]["url"],
                                      (10, (grid_height - 10)),
                                      cv.LINE_AA,
                                      fontScale=1,
                                      thickness=3,
                                      color=(0, 0, 0))

                img_grid_rz = cv.resize(
                    img_grid, (config_file["display"]["resolution"])
                    )

                # give grid to local ui if enabled
                if q_img_grid_local is not None:
                    q_img_grid_local.put(img_grid_rz)

# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import time
from multiprocessing import Event, Queue
from queue import Empty, Full


from deffcode import FFdecoder


def frameget(source: str,
             source_format: str,
             q_img: Queue,
             q_logging: Queue,
             interval: int,
             event: Event
             ):
    """
    Is a producer for analytics()
    continuously reads a video stream to avoid grey-screen errors,
    and puts a frame in a queue for the consumer function
    every frame interval.

    Parameters
    ----------
        source (str): The address/URI/path where FFMPEG will find the source.
        source_format (str): The appropriate FFMPEG demuxer for the source.
        q_img (mp.Manager.Queue): Takes images produced by frameget.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        interval (int): The number of frames that should be skipped, between
            frames that should be processed.
        event (mp.Manager.Event): Cross-process flag, to initiate termination.
    """
    q_logging.put(("info",
                   f"frameget started for {source}"
                   ), timeout=1)
    # define the video decoder parameters
    if source_format is not None:
        if source_format == "rtsp":
            ffparams = {"-rtsp_transport": "tcp",
                        "-vcodec": "hevc_cuvid"}
        else:
            ffparams = {"-d": "auto"}

    decoder = FFdecoder(source,
                        frame_format="bgr24",
                        verbose=False,
                        **ffparams).formulate()
    k = interval

    # grab frames from decoder
    for frame in decoder.generateFrame():
        if frame is None:
            q_logging.put(("debug",
                           "No frames in decoder"
                           ), timeout=0.1)

            continue

        k += 1
        # don't try putting new images in a full queue
        if k > interval and not q_img.full():
            k = 0
            try:
                # avoid locking the process while waiting for a queue to responds
                q_img.put(frame, timeout=0.1)
            except Full:
                time.sleep(2)
                continue
            except Exception as e:
                q_logging.put(("error",
                               f"frameget encountered error {e}"
                               ), timeout=0.1)

                continue

            q_logging.put(
                ("debug",
                 f"video read queue for {source} has {q_img.qsize()} elements"
                 ), timeout=0.1)

        if event.is_set():  # terminate the decoder
            decoder.terminate()
            q_logging.put(("info",
                           "source ingestion ended"
                           ), timeout=0.1)
            break

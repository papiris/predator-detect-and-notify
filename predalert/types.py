from pydantic import AnyHttpUrl, BaseModel
from typing_extensions import TypedDict


# Start of types taken from WebPush-py library (MIT, Copyright (c) 2023 Alexey)
# https://github.com/delvinru/webpush-py/blob/main/webpush/types.py
class WebPushKeys(BaseModel):
    auth: str
    p256dh: str


class WebPushSubscription(BaseModel):
    endpoint: AnyHttpUrl
    keys: WebPushKeys


# a little bit of ugly but ok
WebPushHeaders = TypedDict(
    "WebPushHeaders", {"content-encoding": str, "ttl": str, "authorization": str}
)


class WebPushMessage(BaseModel):
    encrypted: bytes
    headers: WebPushHeaders

# End of types taken from WebPush-py library

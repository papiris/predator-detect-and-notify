# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import queue
import time
from multiprocessing import Event, Queue
from queue import Empty, Full


import torch
from sahi import AutoDetectionModel
from sahi.predict import get_sliced_prediction
from sahi.utils.cv import visualize_object_predictions
import cv2 as cv
from base64 import b64encode


def analytics(config_file:dict,
              q_img: Queue,
              q_analyzed_img_desktop: Queue,
              q_notifications_desktop: Queue,
              q_analyzed_img_web: Queue,
              q_notifications_web: Queue,
              q_logging: Queue,
              event: Event
              ) -> None:
    """
    Is a consumer for frameget() and a producer for desktop and webui
    gets an image from the queue every time interval (sleeping avoids blocking).
    Performs object detection/prediction using an ML model and puts the result,
    which is an annotated frame with named bounding boxes and confidence values,
    in the result queue.

    Parameters
    ----------
        q_img (mp.Manager.Queue): Contains images
            produced by ingest_sources.frameget.
        q_result (mp.Manager.Queue): Contains images annotated with
            bounding boxes for detected objects.
        q_notifications (mp.Manager.Queue): Each item is a list
            of detected objects in the analyzed image.
        q_logging (mp.Manager.Queue): Takes log messages produced by frameget.
        interval (int): The number of frames that should be skipped, between
            frames that should be processed.
        detection_config (dict): Configuration values for object detection
            taken from config.yaml.
        event (mp.Manager.Event): Cross-process flag, to initiate termination.
    """
    q_logging.put(("info", "analytics started"), timeout=1)
    detected_classes = []
    bbox_list = []
    detection_model = AutoDetectionModel.from_pretrained(
        model_type=config_file["detection"]["model"]["type"],
        model_path=config_file["detection"]["model"]["path"],
        confidence_threshold=config_file["detection"]["confidence_threshold"],
        device=torch.device('cuda' if torch.cuda.is_available() else 'cpu'))

    now = time.time()
    while not event.is_set():
        time.sleep(1)
        try:
            frame = q_img.get(timeout=0.1)

        except queue.Empty:
            q_logging.put(
                ("debug", "Analytics encountered error: empty queue"))
            continue

        except Exception as e:
            q_logging.put(("warning", f"Analytics encountered error {e}"))
            continue

        else:
            if time.time() - now > 2:
                now = time.time()
                try:
                    result = get_sliced_prediction(
                        frame,
                        detection_model,
                        slice_height=config_file["detection"]["sahi_window"],
                        slice_width=config_file["detection"]["sahi_window"],
                        overlap_height_ratio=0.2,
                        overlap_width_ratio=0.2
                        )
                except Exception as e:
                    q_logging.put(("warning", f"Analytics encountered error {e}"))

                else:
                    for r in result.object_prediction_list:
                        # ignore "person" class, to avoid processing PII
                        if r.category.name == "person":
                            # cover person with opaque rectangle for privacy
                            privacy_block_start = (round(r.bbox.minx), round(r.bbox.miny))
                            privacy_block_end = (round(r.bbox.maxx), round(r.bbox.maxy))
                            print(privacy_block_start)
                            print(privacy_block_end)

                            color = (0, 0, 0)  # Black color in BGR
                            thickness = -1  # fill the entire shape
                            frame = cv.rectangle(
                                frame,
                                privacy_block_start,
                                privacy_block_end,
                                color,
                                thickness
                                )

                            result.object_prediction_list.remove(r)

                        else:
                            detected_classes.append(r.category.name)
                            coordinates = (
                                r.bbox.minx,
                                r.bbox.miny,
                                r.bbox.maxx,
                                r.bbox.maxy
                                )
                            bbox_list.append(coordinates)

                    if detected_classes:  # don't flood with notifications
                        if q_notifications_desktop:
                            q_notifications_desktop.put(detected_classes)
                        if q_notifications_web:
                            q_notifications_web.put(detected_classes)
                        detected_classes.clear()

                    viz_pred = visualize_object_predictions(
                        image=frame,
                        object_prediction_list=result.object_prediction_list,
                        hide_labels=False)

                    if q_analyzed_img_web:
                        # encode image to efficient format
                        ret, image = cv.imencode(
                            ".webp", viz_pred["image"],
                            params=[cv.IMWRITE_WEBP_QUALITY, 20]
                            )
                        if not ret:
                            q_logging.put(
                                ("warning", "Couldn't webp encode image"))

                        # encode image to base64 string in utf-8 format
                        # to send over network
                        try:
                            base64_image = b64encode(image).decode("utf-8")
                        except Exception as e:
                            q_logging.put(
                                ("warning", "couldn't base64 encode image"))
                        else:
                            try:
                                q_analyzed_img_web.put(base64_image, timeout=0.5)
                            except queue.Full:
                                q_logging.put(
                                    ("debug", "Analytics encountered error: full q_img_web"))
                                continue

                if q_analyzed_img_desktop:
                    try:
                        q_analyzed_img_desktop.put(viz_pred["image"], timeout=0.5)
                    except queue.Full:
                        q_logging.put(
                            ("debug", "Analytics encountered error: full q_img_desktop"))
                        continue

                bbox_list.clear()

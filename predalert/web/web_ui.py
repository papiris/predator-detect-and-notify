# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import aiohttp
import asyncio
import json
import signal

import openziti
from sse_starlette.sse import EventSourceResponse
from hypercorn.config import Config
from hypercorn.asyncio import serve

from pywebpush import WebPushException, webpush

from fastapi import FastAPI, Path
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse, StreamingResponse
from fastapi.requests import Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from multiprocessing import Queue, Event
from queue import Empty
from typing import Optional, Annotated
from sqlmodel import SQLModel, Session, select

from predalert.database import create_db_and_tables, engine
from predalert.models import Subscriptions
from predalert.types import WebPushSubscription
from predalert.web.config import webConfigHolder

"""
FastAPI is restrictive by default, for security.
Therefore, we must declare what requests the app should accept.
"""
middleware = [
    Middleware(CORSMiddleware,
               allow_origins=[
                   'https://localhost',
                   'http://localhost',
                   'http://predalert.ziti'
                   ],
               allow_methods=['GET', 'POST']
               )
]

app = FastAPI(middleware=middleware)

# configuration holder for various web related functions
web_config = webConfigHolder()

# Declare path to templates for Jinja2
templates = Jinja2Templates(
    directory="predalert/web/templates")

# Provide access to directory containing static files, and mount it
static_dir = "predalert/web/static"
app.mount("/static", StaticFiles(directory=static_dir), name="static")


@app.route("/")
async def index(request: Request):
    """
    Serves the homepage, with semi-dynamically defined
    video streams.

    Parameters
    ----------
        request (web request): This function triggers when
            the homepage is visited, i.e. when a request is run.
    """
    stream_indices = web_config.read_key('stream_indices')

    # Render the template with dynamic content
    return templates.TemplateResponse(
        "index.html", {"request": request, "stream_indices": stream_indices})


async def img_stream_generator(request: Request, q_img: Queue):
    """
    """
    q_logging = web_config.read_key('q_logging')
    event = web_config.read_key('event')

    while not event.is_set():
        if await request.is_disconnected():
            q_logging.put(("info", "client disconnected"), timeout=0.5)
            break
        try:
            base64_image = q_img.get(timeout=0.2)
        except Empty as e:
            await asyncio.sleep(0.01)
            continue
        except Exception as e:
            await q_logging.put(("error", f"web_ui.img_stream_generator has error: {e}"))
        else:
            yield base64_image


@app.get("/stream/{stream_idx}")
async def stream_images(stream_idx: Annotated[int, Path(title="index of the image queue")],
                        request: Request):
    """
    Custom frame producer for WebGear.
    Transcodes images from q_result to bytes,
    and yields them to the web process.
    Due to peculiarities with web frameworks (django, flask, uvicorn)
    when called as the target of multiprocessing,
    arguments apart from q_result must be passed through the <config> object.

    Parameters
    ----------
        q_img_grid_web (mp.Manager.Queue): Images to show on the web UI.
        q_result (mp.Manager.Queue): Analyzed, annotated images to display.
            One queue per analytics process.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        event (mp.Manager.Event): Cross-process flag, to initiate termination

    Returns
    ----------
        byte-formatted responses to facilitate network transfer,
            either a plaintext errorcode, or an encoded image.
    """
    q_logging = web_config.read_key('q_logging')
    event = web_config.read_key('event')
    stream_q_objects = web_config.read_key('stream_q_objects')
    q_img = stream_q_objects[stream_idx]

    event_generator = img_stream_generator(request, q_img)

    return EventSourceResponse(event_generator)


@app.get("/api/v2/key")
async def get_public_key(request) -> JSONResponse:
    """
    Provide the VAPID public key to client-side
    javascript webpush ServiceWorker

    Parameters
    ----------
        GET request with no extra parameters

    Returns
    ----------
        JSON response with a dict containing the VAPID public key.
    """
    return JSONResponse(content={
        "public_key": config_file['alert']['push']['vapid_app_server_key']
        })


@app.post("/api/v2/subscribe")
async def subscribe_user(request: WebPushSubscription) -> JSONResponse:
    """
    Receive WebPush subscription info from client,
    verify its formatting and
    extract the json object from the request body,
    and add the subscription to the database.
    Try to do a WebPush notification, to check operation.

    Parameters
    ----------
        POST request conforming to the WebPushSubscription standard,
            with a body containting a pushManager|.subscribe object
            that has been JSON.stringify()-ied.
            No need to convert from base64 to uint8 array
            using the well-known urlB64ToUint8Array()
            function.

    Returns
    ----------
        JSON response with a status code as well as the json representation
            of the request body of the parameter.
    """
    q_logging = web_config.read_key('q_logging')
    subscription_info = await request.json()
    data = "subscribed to push from backend!"
    try:
        """
        TODO: load more claims from config
        Webpush requires that the person/org responsible
        for sending the notification, bundles their email address
        with the notification; to be notified in case of abuse
        """
        webpush(subscription_info,
                data,
                vapid_private_key=config_file['alert']['push']['vapid_private_key'],
                vapid_claims={
                    "sub": f"mailto:{config_file['alert']['push']['contact_address']}",
                    }
                )
    except WebPushException as e:
        q_logging.put(("info", f"subscribe_user encountered error {e}"))
        if e.response and e.response.json():
            extra = e.response.json()
            q_logging.put(
                "info",
                "Remote service replied with a "
                f"{extra.code}:{extra.errno}, {extra.message}"
                )
    else:
        with Session(engine) as session:
            session.add(
                Subscriptions(
                    id=id(subscription_info),
                    endpoint=subscription_info['endpoint'],
                    expirationTime=subscription_info['expirationTime'],
                    auth=subscription_info['keys']['auth'],
                    p256dh=subscription_info['keys']['p256dh']
                    ))
            session.commit()

    return JSONResponse(content={"status": "ok",
                                 "message": subscription_info})


shutdown_event = asyncio.Event()


def _signal_handler(*_: any) -> None:
    shutdown_event.set()


def run_app(config_file: dict,
            q_analyzed_img_web_dict: dict[Queue],
            q_notification_web: Queue,
            q_logging: Queue,
            ziti_cfg: dict,
            event: Event) -> None:
    """
    Serves the web UI.
    Sets a custom frame generator to use.

    Parameters
    ----------
        q_img_grid_web (mp.Manager.Queue): Images to show on the web UI.
        q_result_dict (dict of mp.Manager.Queue objects):
            Analyzed, annotated images to be gridified.
            One queue per analytics process.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        ziti_cfg: configuration for openziti bindings
        event (mp.Manager.Event): Cross-process flag,
            to initiate termination.
    """

    web_config.modify_key("q_logging", q_logging)
    web_config.modify_key("q_result_dict", q_analyzed_img_web_dict)
    web_config.modify_key("q_web_notification", q_notification_web)
    web_config.modify_key("event", event)

    stream_q_objects = []
    stream_indices = []
    for index, q_analyzed_img in enumerate(q_analyzed_img_web_dict.values()):
        stream_indices.append(index)
        stream_q_objects.append(q_analyzed_img)
        q_logging.put(f"web_ui queue: {q_analyzed_img}")

    web_config.modify_key("stream_indices", stream_indices)
    web_config.modify_key("stream_q_objects", stream_q_objects)

    # prep to send push notifications
    create_db_and_tables()

    # Configuration for hypercorn
    config = Config()
    config.bind = [
        f"{config_file['web_ui']['url']}:{config_file['web_ui']['port']}"
        ]
    config.errorlog = "-"  # print errorlog to stderr
    config.loglevel = "INFO"

    # Point these variables to  SSL cert/key files for HTTP/2 in hypercorn,
    # which is necessary to support more than 6 concurrent SSE connections per domain
    if config_file['web_ui']['ssl']['enabled']:
        config.certfile = config_file['web_ui']['ssl']['cert']
        config.keyfile = config_file['web_ui']['ssl']['key']

    # for openziti
    if config_file['web_ui']['zero_trust']['enabled']:
        openziti.monkeypatch(
            bindings={
                (config_file['web_ui']['url'], int(config_file['web_ui']['port'])): ziti_cfg
                }
            )

    # Start hypercorn
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGTERM, _signal_handler)
    loop.run_until_complete(
        serve(app, config, shutdown_trigger=shutdown_event.wait)
    )


if __name__ == "__main__":
    run_app()

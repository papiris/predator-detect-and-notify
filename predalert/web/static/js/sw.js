console.log("This message is from the service worker");

const urlB64ToUint8Array = base64String => {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

/* Get VAPID public key from endpoint on server and
 * Convert it to Uint8 array because the VAPID spec says so.
 */
const getVapidPublicKey = async () => {
  try {
    // Get the key from the server endpoint
    const response = await fetch("http://predalert.ziti:8443/api/v2/key");
    if (!response.ok) {
      throw new Error("Failed to fetch VAPID public key" + response.status);
    }
    // From full response, extract only json message
    const applicationServerPublicKey = await response.json();
    //Send only value from public_key json key, to be converted
    const UINT8applicationServerPublicKey = await urlB64ToUint8Array(
      applicationServerPublicKey["public_key"])

    return applicationServerPublicKey["public_key"]
  } catch (error) {
    console.error("Error fetching VAPID public key:", error.message);
    throw error; // Propagate the error
    }
}


const saveSubscriptionToBackEnd = async (subscription) => {
  try {
    const response = await fetch('http://predalert.ziti:8443/api/v2/subscribe', {
      method: 'post',
      headers: { 'Content-Type': "application/json" },
      body: JSON.stringify(subscription)
    });
    if (!response.ok) {
      throw new Error("Failed to save subscription to backend" + response.status);
    }
    console.log('const response in saveSubscriptionToBackEnd works')
    return response.json()
  } catch (error) {
    console.log("Error saving subscription to backend:", error.message);
    throw error; // Propagate the error
  }
};

self.addEventListener('activate', async (e) => {
  console.log('activate triggered')
  let subscriptionData = null;
  try {
    console.log('Subscription in sw activate:', subscriptionData);
    const UINT8applicationServerPublicKey = await getVapidPublicKey();
    console.log('applicationServerPublicKey:', UINT8applicationServerPublicKey)
    const subscription = await self.registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: UINT8applicationServerPublicKey,

    });

    subscriptionData = {
      endpoint: subscription.endpoint,
      keys: {
          p256dh: subscription.getKey('p256dh'),
          auth: subscription.getKey('auth')

      }
    };

    const response = await saveSubscriptionToBackEnd(subscription)
    console.log('Response from saveSubscriptionToBackEnd:', response);
  } catch (error) {
    console.error("Error in activate event:", error.message);
    }
});

self.addEventListener('push', (e) => {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
    // Notification permissions are not granted yet, do nothing.
      console.log('Recieved push event before permissions were granted.')
    return;
  }

  // Check if there is data available in the push event
    if (e.data) {
    console.log('This push event has data: ', e.data.text());
    } else {
    console.log('This push event has no data.');
    };


  // const pushData = e.data ? e.data.json() : {};

  // console.log(pushData);
  const options = {
    body: e.data.text() || 'No notification body, using default'
  };
  e.waitUntil(
    self.registration.showNotification('Predalert', options)
  );
});

// Add event listener to handle notification click
self.addEventListener('notificationclick', (e) => {
  e.notification.close();

  // TODO: add custom logic here based on the notification's data
  // For example, open  URL when the notification is clicked
  const urlToOpen = 'https://localhost:8443';
  event.waitUntil(
    clients.openWindow(urlToOpen, '_blank')
  );
});



'use strict';

/*handles fullscreen workflow*/
function toggleFullScreen(id) {
    const element = document.getElementById(id);
    screenfull.toggle(element)
}


const pushToggleButton = document.getElementById("pushButton");


const detectSupportedFeatures = () => {
  if (!('serviceWorker' in navigator)) {
    // Service Worker isn't supported on this browser, disable or hide UI.
    throw new Error("No support for service worker in browser")
  }

  if (!('PushManager' in window)) {
    // Push isn't supported on this browser, disable or hide UI.
    throw new Error("No support for push notifications in browser")
  }

  if (!('Notification' in window)) {
    throw new Error("No support for Notifications API")
  }
}

const registerSW = async () => {
  const registration = await navigator.serviceWorker.register('/static/js/sw.js', {scope: '/static/js/'})
  .then(function(registration) {
    console.log('Service worker successfully registered with scope', registration.scope);
  return registration
  })
  .catch(function(err) {
    console.error('Unable to register service worker.', err);
  });
}

const requestNotificationPermission = async () => {
  const permission = await Notification.requestPermission();

  if(permission !== 'granted') {
    throw new Error("Notification permission not granted")
  } else {
    new Notification("Push notifications enabled!")
  }
  return permission
}


function askPermission() {
  return new Promise(function(resolve, reject) {
    const permissionResult = Notification.requestPermission(function(result) {
      resolve(result);
    });

    if (permissionResult) {
      permissionResult.then(resolve, reject);
    }
  })
  .then(function(permissionResult) {
    if (permissionResult !== 'granted') {
      throw new Error('We weren\'t granted permission.');
    }
  });
}


function getNotificationPermissionState() {
  if (navigator.permissions) {
    return navigator.permissions.query({name: 'notifications'})
    .then((result) => {
      return result.state;
    });
  }

  return new Promise((resolve) => {
    resolve(Notification.permission);
  });
}


const serviceWorkerMain = async () => {
  await registerSW()
}

/*initializes the UI for push subscription*/
pushToggleButton.addEventListener('click', () => {
    // pushButton.disabled = true;
    /*const currentPermission = Notification.permission
    if (currentPermission === 'granted') {
      navigator.serviceWorker.getRegistrations().then(registrations => {
        for (const registration of registrations) {
          registration.unregister();
        }
      });
    }
    else { */
  requestNotificationPermission()
  serviceWorkerMain()
    // }
})

  /*

    } else {
      askPermission();
      // subscribeUserToPush();
      // sendSubscriptionToBackEnd();

  });

  // Set the initial subscription value
  SWReg.pushManager.getSubscription()
  .then(function(subscription) {
    isSubscribed = !(subscription === null);

    // updateSubscriptionOnServer(subscription);

    if (isSubscribed) {
      console.log('User IS subscribed.');
    } else {
      console.log('User is NOT subscribed.');
    }

    updateBtn();
  });
}
*/

/*button to toggle web push notifications
function updateBtn() {
  if (isSubscribed) {
    pushButton.textContent = 'Disable Push Messaging';
  } else {
    pushButton.textContent = 'Enable Push Messaging';
  }

  pushButton.disabled = false;
};

*/

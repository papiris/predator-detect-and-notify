# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import queue
import time
from multiprocessing import Event, Queue

from pywebpush import WebPushException, webpush
from sqlmodel import Session, select

from predalert.models import Subscriptions
from predalert.web.web_ui import engine


def send_push_notifications(config_file,
                            q_web_notification: Queue,
                            q_logging: Queue,
                            event: Event
                            ):
    """
    Send notifications from backend to existing subscribers
    """

    while not event.is_set():
        try:
            detected_cls = q_web_notification.get(timeout=0.2)
        except queue.Empty:
            q_logging.put("debug", "q_web_notification is empty")
            continue
        else:
            # send alert to all subscribed clients
            with Session(engine) as session:
                subscriptions = session.exec(select(Subscriptions))
                for result in subscriptions:
                    summarized_list = [f"{detected_cls.count(item)} {item}"
                                       for item in set(detected_cls)]
                    try:
                        webpush(
                            subscription_info={
                                "endpoint": result.endpoint,
                                "keys": {
                                    "p256dh": result.p256dh,
                                    "auth": result.auth
                                    }},
                                data=f"Detected classes: {summarized_list}",
                                vapid_private_key=config_file['alert']['push']['vapid_private_key'],
                                vapid_claims={
                                    "sub": f"mailto:{config_file['alert']['push']['contact_address']}",
                                    }
                                )

                    except WebPushException as e:
                        q_logging.put("info", f"subscribe_user encountered error {e}")
                        if e.response and e.response.json():
                            extra = e.response.json()
                            q_logging.put(
                                "info",
                                "Remote service replied with a "
                                f"{extra.code}:{extra.errno}, {extra.message}"
                                )
                        continue
                    except Exception as e:
                        q_logging.put("debug", f"subscribe_user encountered error {e}")

                time.sleep(10)

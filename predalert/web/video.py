# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import asyncio
import queue
from multiprocessing import Queue

import cv2 as cv
from fastapi.responses import StreamingResponse


async def frame_producer_dynamic(q_result: Queue, web_config: dict):
    """
    Custom frame producer for WebGear.
    Transcodes images from q_result to bytes,
    and yields them to the web process.
    Due to peculiarities with web frameworks (django, flask, uvicorn)
    when called as the target of multiprocessing,
    arguments apart from q_result must be passed through the <config> object.

    Parameters
    ----------
        q_img_grid_web (mp.Manager.Queue): Images to show on the web UI.
        q_result (mp.Manager.Queue): Analyzed, annotated images to display.
            One queue per analytics process.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        event (mp.Manager.Event): Cross-process flag, to initiate termination

    Returns
    ----------
        byte-formatted responses to facilitate network transfer,
            either a plaintext errorcode, or an encoded image.
    """
    q_logging = web_config.read_key('q_logging')
    event = web_config.read_key('event')
    while not event.is_set():
        try:
            # read frame from queue
            unencoded_image = q_result.get_nowait()
        except queue.Empty:
            await asyncio.sleep(0.1)
            continue

        except Exception as e:
            q_logging.put(("error", f"web_ui.get_frames has error: {e}"))
            await asyncio.sleep(0.001)
            yield (b"--frame\r\n"
                b"Content-Type: text/plain\r\n\r\n\r\n")

        else:
            try:
                # cv.imencode() returns ret (boolean) as well as the encoded image.
                # To use the .tobytes() function directly, only return the second.
                encoded_image = cv.imencode(".jxl", unencoded_image)[1].tobytes()
            except Exception as e:
                q_logging.put(("error", f"web_ui.get_frames has error: {e}"))
                await asyncio.sleep(0.001)
            # yield frame in byte format
            await asyncio.sleep(0.001)
            yield (b"--frame\r\n"
                b"Content-Type:image/jpeg\r\n\r\n" + encoded_image + b"\r\n")


async def dynamic_video_response(q_result: Queue) -> StreamingResponse:
    """
    Return an async video streaming response for frame_producer_dynamic().

    Parameters
    ----------
        q_result (mp.Manager.Queue): Contains analyzed,
            annotated images to be displayed.
            One queue per analytics process.

    Returns
    ----------
        StreamingResponse that continuously replaces the previous image
            in a frame with the newest one in the queue.
    """
    # assert scope["type"] in ["http", "https"]
    await asyncio.sleep(0.001)
    return StreamingResponse(
        (frame_producer_dynamic(q_result)),
        media_type="multipart/x-mixed-replace; boundary=frame")


async def generate_video_urls(web_config: dict) -> list[str]:
    """
    Makes routes and streamingResponses
    for the image queue objects within
    q_result_dict.

    Parameters
    ----------
        web_config (dict):
            convenience structure to pass objects and variables between
                different classes, functions and processes.
        q_result_dict (dict of q_result):
            q_result (mp.Manager.Queue): Contains analyzed,
                annotated images to be displayed.
                One queue per analytics process.

    Returns
    ----------
        video_urls (list): Contains urls for where frontend html can GET
            analyzed, annotated image streams.
    """
    q_result_dict = web_config.read_key("q_result_dict")
    q_logging = web_config.read_key('q_logging')
    event = web_config.read_key('event')
    video_urls = []
    video_stream_objects = []
    for index, q_result in enumerate(q_result_dict.values()):
        async def video_stream(request, q_result=q_result):
            return StreamingResponse(
                frame_producer_dynamic(q_result, web_config),
                media_type='multipart/x-mixed-replace; boundary=frame')

        video_urls.append(f"/video{index}")
        video_stream_objects.append(video_stream)

    return video_urls, video_stream_objects

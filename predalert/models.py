from typing import Optional

from sqlmodel import Field, SQLModel


# Set up database table for WebPush subscriptions
class Subscriptions(SQLModel, table=True):
    id: Optional[str] = Field(default=None, primary_key=True)
    endpoint: str
    expirationTime: Optional[int] = Field(default="null")
    auth: str
    p256dh: str

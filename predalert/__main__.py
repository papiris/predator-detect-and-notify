# Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later

# This program takes an arbitrary number of rtsp (or other video) sources,
# performs object detection and shows the results in a grid.

from os import environ
import multiprocessing as mp
import time
from pathlib import Path
import tomllib

from predalert.detection import analytics
from predalert.ingest_sources import frameget
from predalert.log_handler import get_log_messages


def main():

    # Load configuration file
    path = Path(environ['PREDALERT_CONFIG_DIR'], "config.toml")
    try:
        with path.open("rb") as f:
            config_file = tomllib.load(f)
    except tomllib.TOMLDecodeError as e:
        print(f"ERR: Config file couldn't load due to error: {e}")


    mp.set_start_method("forkserver", force=True)
    # assign enabled sources to variables
    sources = []
    source_formats = []
    for source_config in config_file['sources'].values():
        # Check if the source is enabled
        if source_config['enabled']:
            # Create variables from each of the values
            if source_config["format"]:
                source_format = source_config['format']
                source_formats.append(source_format)
            path = source_config['path']
            sources.append(path)

    max_size_queue = 7  # max elements in the queue. Any more than 8, and it'd be old.
    interval = 10  # interval of video frames between frames analysed.

    # Create manager queues
    manager = mp.Manager()

    # Queue for log messages
    # Producers: MP all processes
    # Consumer: SC logging
    q_logging = manager.Queue()

    # Dict of queues for decoded images from video sources
    # One queue per source
    # Producer: SP frameget
    # Consumer: SC analytics
    q_img_dict = {src: manager.Queue(max_size_queue) for src in sources}

    if config_file["display"]["enabled"]:
        # Dict of queues for analyzed, annotated images
        # One queue per source
        # Producer: SP analytics
        # Consumer: SC make_image_grid
        q_analyzed_img_desktop_dict = {src: manager.Queue(max_size_queue) for src in sources}

        # Queue for desktop notifications
        # Producers: MP analytics
        # Consumer: SC desktop_notifications
        q_notification_desktop = manager.Queue(max_size_queue)

        # Queue for analysed, annotated, then gridified images
        # Producer: SP make_image_grid
        # Consumer: SC show_image_grid
        q_img_grid_desktop = manager.Queue(max_size_queue)
    else:
        q_analyzed_img_desktop_dict = {src: None for src in sources}
        q_notification_desktop = None
        q_img_grid_desktop = None

    if config_file['web_ui']['enabled']:
        # Dict of queues for analyzed, annotated images
        # One queue per source
        # Producer: SP analytics
        # Consumer: SC web.video
        q_analyzed_img_web_dict = {src: manager.Queue(max_size_queue) for src in sources}

    else:
        q_analyzed_img_web_dict = {src: None for src in sources}

    if config_file['alert']['push']['enabled']:
        # Queue for web push notifications
        # Producers: MP analytics
        # Consumer: SC web_ui
        q_notification_web = manager.Queue(2)

    else:
        q_notification_web = None

    if config_file['web_ui']['zero_trust']['enabled']:
        # Pass ziti config as proxy object, because queues break otherwise
        ziti_cfg = manager.dict(
            ztx=f"{config_file['web_ui']['zero_trust']['identity']}",
            service=f"{config_file['web_ui']['zero_trust']['service_name']}"
            )
    else:
        ziti_cfg = manager.dict(ztx=None, service=None)

    # trigger to terminate child processes
    event = manager.Event()

    q_logging.put(("info", "program started"), timeout=0.1)

    # Create and start subprocesses to read videos, analyze frames,
    # and display frames
    processes = []
    try:
        # start logging process
        logging_process = mp.Process(
            target=get_log_messages,
            args=(
                q_logging,
                event
                  )
            )
        logging_process.start()
        processes.append(logging_process)

    except Exception as e:
        q_logging.put(
            ("warning",
             f"Exception occurred when starting logging_process {e}"), timeout=0.1)

    try:
        # start one video reading process per source
        vid_read_jobs = [mp.Process(
            target=frameget,
            args=(
                source,
                source_format,
                q_img,
                q_logging,
                interval,
                event
                ),
            daemon=True
            ) for source, source_format, q_img in zip(
                sources,
                source_formats,
                q_img_dict.values()
                )]

    except Exception as e:
        q_logging.put(
            ("warning",
             f"Exception occurred when starting vid_read_jobs {e}"), timeout=0.1)

    try:
        # start one analytics process per source
        img_analysis_jobs = [mp.Process(
            target=analytics,
            args=(
                config_file,
                q_img,
                q_analyzed_img_desktop,
                q_notification_desktop,
                q_analyzed_img_web,
                q_notification_web,
                q_logging,
                event
                ),
            daemon=True
            ) for q_img, q_analyzed_img_desktop, q_analyzed_img_web in zip(
                q_img_dict.values(),
                q_analyzed_img_desktop_dict.values(),
                q_analyzed_img_web_dict.values()
                )]

    except Exception as e:
        q_logging.put(
            ("warning",
             f"Exception occurred when starting img_analysis_jobs {e}"), timeout=0.1)

    else:
        # Start jobs and add them to list for graceful termination
        for jobs in zip(vid_read_jobs, img_analysis_jobs):
            jobs[0].start()
            jobs[1].start()
            processes.append(jobs[0])
            processes.append(jobs[1])

    if config_file["display"]["enabled"]:
        try:
            from predalert.desktop.image_grid import make_image_grid
            # start one process to make images into a grid
            image_grid_process = mp.Process(
                target=make_image_grid,
                args=(
                    config_file,
                    q_analyzed_img_desktop_dict,
                    q_img_grid_desktop,
                    q_logging,
                    event
                    ),
                daemon=True
                )
            image_grid_process.start()
            processes.append(image_grid_process)

        except Exception as e:
            q_logging.put(
                ("warning",
                 f"Exception occurred when starting image_grid_process {e}"), timeout=0.1)

        try:
            # Start one process to display a local UI
            from predalert.desktop.desktop_ui import show_image_grid
            display_process = mp.Process(
                target=show_image_grid,
                args=(
                    q_img_grid_desktop,
                    q_logging,
                    event
                    ),
                daemon=True
                )
            display_process.start()
            processes.append(display_process)

        except Exception as e:
            q_logging.put(
                ("warning",
                f"Exception occurred when starting notification_process {e}"), timeout=0.1)

    # start one process to produce local desktop notifications
    if config_file["alert"]["desktop-notify"]["enabled"]:
        try:
            from predalert.desktop.desktop_alert import desktop_notifications
            notification_process = mp.Process(
                target=desktop_notifications,
                args=(
                    q_notification_desktop,
                    q_logging,
                    event
                    ),
                daemon=True
                )
            notification_process.start()
            processes.append(notification_process)

        except Exception as e:
            q_logging.put(
                ("warning",
                 f"Exception occurred when starting notification_process {e}"), timeout=0.1)

    # start one process to serve a UI over the network
    if config_file["web_ui"]["enabled"]:
        try:
            from predalert.web.web_ui import run_app
            web_ui_process = mp.Process(
                target=run_app,
                args=(config_file,
                      q_analyzed_img_web_dict,
                      q_notification_web,
                      q_logging,
                      ziti_cfg,
                      event
                      ),
                daemon=False
                )

            web_ui_process.start()
            processes.append(web_ui_process)

        except Exception as e:
            q_logging.put(
                ("warning",
                 f"Exception occurred when starting web_ui_process {e}"), timeout=0.1)

    # start one process to send out push notifications
    if config_file['alert']['push']['enabled']:
        try:
            from predalert.web.push_alert import send_push_notifications
            push_alert_process = mp.Process(
                target=send_push_notifications,
                args=(config_file,
                      q_notification_web,
                      q_logging,
                      event
                      ),
                daemon=False
                )
            push_alert_process.start()
            processes.append(push_alert_process)

        except Exception as e:
            q_logging.put(
                ("warning",
                 f"Exception occurred when starting push_alert_process {e}"), timeout=0.1)

    # loop to check whether ctrl-c has been pressed, and exit if that is the case
    while True:
        try:
            time.sleep(0.5)

        except KeyboardInterrupt:
            q_logging.put(("info", "exited main loop"), timeout=0.1)
            time.sleep(0.5)
            event.set()

            break

    # wait a little for processes to terminate gracefully
    for proc in processes:
        proc.join(1)


if __name__ == '__main__':
    main()

# This file is part of Predalert
# Copyright (c) 2023-2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later

import logging
from multiprocessing import Lock, Queue
from queue import Empty
import time


# Make logging facility
logger = logging.getLogger("Predalert")

# Set minimum loglevel to be printed
logger.setLevel(logging.DEBUG)

# Instantiate multiprocessing lock
p_lock = Lock()


def init_logging(q_logging):
    """
    Creates a handler that will send log messages to the queue

    Parameters
    ----------
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
    """
    handler = logging.handlers.QueueHandler(q_logging)
    logger.addHandler(handler)


def get_log_messages(q_logging, event):
    """
    Consume log messages put into the log queue.

    Parameters
    ----------
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        event (mp.Manager.Event): Cross-process flag, to initiate termination.
    """
    k = 0
    while True:
        try:
            level, log_message = q_logging.get(timeout=0.1)
        except Empty:
            time.sleep(0.2)
            continue
        except Exception as e:
            mp_safe_logging('WARN', f"exception in log_handler: {e}")
        else:
            mp_safe_logging(level, log_message)

        # Handle remaining logs before terminating
        if event.is_set():
            k += 1
            if k >= 20:
                break



def mp_safe_logging(level, message):
    """
    standard logging is not thread-safe OR multiprocessing safe,
    and can lead to lock-up.
    This function ensures only one process prints to
    a file descriptor (file, socket, stdout) at a time.

    Parameters
    ----------
        level (str): loglevel of the message. Can be any of
            {debug, info, warning, error, critical},
            listed in order of increasing level.
        q_logging (mp.Manager.Queue): Log messages to be consumed
            by log_handler process.
        event (mp.Manager.Event): Cross-process flag, to initiate termination.
    """
    log_function = getattr(logger, level.lower(), None)
    with p_lock:
        log_function(message)

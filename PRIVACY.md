# GDPR Compliance Guidance
Camera streams are subject to strict privacy laws, and even moreso if they are transmitted across the open internet.

- Run this program only on your own network, using only video streams originating from your local network.
- Encrypt and password-protect the video streams.
- Don't allow the camera to see anything not intended or allowed for video.
- If you have a PTZ camera; don't pan, tilt, or zoom to a point where you breach the previous point
- The program has a filter mechanism which blocks recording if, for whatever reason, a human is in the frame at the same time as an object of interest (predator). This is to strictly avoid processing personal information.
- Clearly mark the area as "camera surveilled" with a sign. It should include the purpose and limitations of the surveillance.

This guide was developed with some guidance from the [Norwegian Data Protection Agency](https://www.datatilsynet.no/en/) but I've not yet absorbed the [Norwegian Digitalisation Agency's guide to responsible AI](https://www.digdir.no/kunstig-intelligens/veiledning-ansvarlig-bruk-og-utvikling-av-kunstig-intelligens/4601). This will come with time, as it approaches a 1.0 release. If you have knowledge in this field, please open an issue or merge request so we can develop this responsibly.

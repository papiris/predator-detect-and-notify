# Predator-Detect-and-Notify

## Introduction
This software uses the state-of-the-art [YOLOv8](https://github.com/ultralytics/ultralytics) object detection and classification neural net to detect predators in a video stream.

It accomplishes this using model weights (soon-to-be) fine-tuned on a range of farm animals and predators.
This software is licenced under AGPL-v3-or-later.

This project is meant for personal (or business) use, within a single network. This is not a saas platform, and making it one is not currently on the roadmap.


## Installation
*This software is Linux-native, and has yet to be tested on Windows. Your mileage on Windows may vary.

1. `$ git clone https://gitlab.com/papiris/predator-detect-and-notify && cd predator-detect-and-notify`

You need `python --version` >3.8 & <=3.11.

2. `$ python3 -m venv .env`

3. `$ source .env/bin/activate`

4. `python3 -m pip install .`

The installation of packages may take a while, having a cup of coffee while you wait is advised.

Your virtual environment is now prepared.



## How to use
1. Find the ip address and path to RTSP stream for your given IP camera(s). [Look for your camera make and model here](https://security.world/rtsp/) or [find the stream URL manually by following this guide](https://superuser.com/questions/1253126/how-do-i-find-the-video-stream-url-of-onvif-cameras-manually/1711576#1711576).
2. [optional] set the IP address of your camera to static in your network router.
3. Change values in config/config.toml and security.toml to suit your environment (important!)
4. Make sure your virtual environment is activated. If not, repeat step 3 of the install process.
5. Run `python -m predalert`

The required computer resources (ram, processing, graphics) scales linearly with the number of sources and their resolution.


## Roadmap
- [ ] fine-tune model weights
-- [ ] on sheep, humans, dogs and lynx
-- [ ] on other classes
- [x] remove hardcodings, migrate to config file
- [x] use multiprocessing to increase performance with many sources
- [x] graceful handling of errors, not let one bad videostream hold back others
- [ ] add motion and proximity logic to determine threat level of predator
- [x] filter out humans from any data processing for privacy reasons
- [ ] add option to record and store clips of predators
- [x] add local notification system
- [x] add webpush notification system
- [ ] add web-based UI for making changes to config file
- [ ] add web-based UI for setting areas of interest (polygons)
- [ ] enable ML offloading (to Coral Accelerator, Codeproject.ai, Deepstack)
- [x] add privacy statement
-- [ ] Improve privacy statement
- [x] comply with AGPL-v3 (source available to all users, even when the program is served over network)
- [x] make deployable in docker or other package format
- [ ] Add integrations with / bridges for various drones for ranged (and autonomous) grazing area surveillance
- [ ] Add integrations with / bridges for various wildlife cameras


## Contributions
This project accepts contributions of many kinds. Due to my employment status (on welfare), monetary contributions are most welcome. I'll get a donation link up soon.
There are many ways to contribute apart from monetarily;
1. code (merge requests)
2. bug reports
3. training material (images/videos)
4. spread the word! I love to hear about users' stories, as well as media recognition. It helps make my case to get more support for the project.


## External libraries
- [YOLOv8](https://docs.ultralytics.com/) (GNU affero General Public License (AGPL-v3))
- [matplotlib](https://matplotlib.org/) (Python Software Foundation License (PSF))
- [openCV](https://github.com/opencv/opencv-python) (Apache Software License (Apache 2.0))
- [numpy](https://numpy.org/) (BSD-3-Clause)
- [deffcode](https://abhitronix.github.io/deffcode/latest/) (Apache Software License (Apache 2.0))
- [RT-DETR](https://github.com/lyuwenyu/RT-DETR) (Apache Software License (Apache 2.0))
- [vidgear](https://abhitronix.github.io/vidgear/latest/) (Apache Software License (Apache 2.0))
